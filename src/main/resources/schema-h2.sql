create table applications_application (
  application_id varchar(36)  not null,
  dt_created     timestamp not null,
  product_name   varchar(255) not null,
  contact_id     varchar(36) not null,
  primary key (application_id)
);
create table applications_contact (
  contact_id varchar(36) not null,
  primary key (contact_id)
);

alter table applications_application
  add constraint fk_applications_application_contact_id foreign key (contact_id) references applications_contact;

create index contact_dt_created_on_applications_application_idx ON applications_application (contact_id, dt_created);

create unique index uniq_applications_application_dt_created on applications_application (contact_id, dt_created);