package applications;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationsServer {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationsServer.class, args);
    }

}
