package applications.services;


import applications.model.Application;

import java.util.Optional;
import java.util.UUID;

public interface ApplicationService {

    Optional<Application> findLastOneByContact(UUID contactId);

}
