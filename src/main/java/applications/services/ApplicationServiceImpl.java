package applications.services;

import applications.controllers.exceptions.EntityNotFoundException;
import applications.model.Application;
import applications.model.Contact;
import applications.repositories.ApplicationsRepository;
import applications.repositories.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class ApplicationServiceImpl implements ApplicationService {

    private final ApplicationsRepository applicationsRepository;

    private final ContactRepository contactRepository;

    @Autowired
    public ApplicationServiceImpl(ApplicationsRepository applicationsRepository, ContactRepository contactRepository) {
        this.applicationsRepository = applicationsRepository;
        this.contactRepository = contactRepository;
    }

    @Override
    public Optional<Application> findLastOneByContact(UUID contactId) {
        if(!contactRepository.existsById(contactId)) {
            throw new EntityNotFoundException(Contact.class, contactId);
        }

        return applicationsRepository.findLastOneByContact(contactId);
    }
}
