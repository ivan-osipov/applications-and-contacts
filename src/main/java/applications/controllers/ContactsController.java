package applications.controllers;

import applications.controllers.dto.ApplicationOutputDto;
import applications.services.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("${api_v1}/contact")
public class ContactsController {

    private final ApplicationService applicationService;

    @Autowired
    public ContactsController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @GetMapping(value = "/{contactId}/applications/last", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ApplicationOutputDto> findLastOneByContact(@PathVariable("contactId") UUID contactId) {
        return applicationService.findLastOneByContact(contactId)
                .map(ApplicationOutputDto::create)
                .map(ResponseEntity.ok()::body)
                .orElse(ResponseEntity.noContent().build());

    }

}
