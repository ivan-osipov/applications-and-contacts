package applications.controllers.dto;

import applications.model.Application;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.time.LocalDateTime;
import java.util.UUID;

@JacksonXmlRootElement(localName = "APPLICATION")
public class ApplicationOutputDto {

    @JsonProperty("CONTACT_ID")
    public final UUID contactId;

    @JsonProperty("APPLICATION_ID")
    public final UUID applicationId;

    @JsonProperty("DT_CREATED")
    public final LocalDateTime createdAt;

    @JsonProperty("PRODUCT_NAME")
    public final String productName;

    private ApplicationOutputDto(UUID contactId, UUID applicationId, LocalDateTime createdAt, String productName) {
        this.contactId = contactId;
        this.applicationId = applicationId;
        this.createdAt = createdAt;
        this.productName = productName;
    }

    public static ApplicationOutputDto create(Application application) {
        return new ApplicationOutputDto(application.getContact().getId(),
                application.getId(),
                application.getCreatedAt(),
                application.getProductName());
    }


}
