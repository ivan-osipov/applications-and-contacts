package applications.repositories;

import applications.model.Application;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface ApplicationsRepository extends CrudRepository<Application, UUID> {

    @Query("select app from apps$Application app where app.contact.id = :contactId and app.createdAt = " +
            "(select max(a.createdAt) from apps$Application a where a.contact.id = :contactId)")
    Optional<Application> findLastOneByContact(@Param("contactId") UUID id);

}
