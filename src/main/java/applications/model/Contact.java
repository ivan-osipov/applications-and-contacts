package applications.model;

import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "APPLICATIONS_CONTACT")
@Entity(name = "apps$Contact")
@AttributeOverride(name = "id", column = @Column(name = "CONTACT_ID"))
public class Contact extends Identifiable {

    @Getter
    @OneToMany(mappedBy = "contact", targetEntity = Application.class)
    protected List<Application> applications = new ArrayList<>();

}
