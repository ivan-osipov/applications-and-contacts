package applications.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "APPLICATIONS_APPLICATION")
@Entity(name = "apps$Application")
@AttributeOverride(name = "id", column = @Column(name = "APPLICATION_ID"))
@Getter@Setter
public class Application extends Identifiable {

    @Column(name = "DT_CREATED")
    protected LocalDateTime createdAt;

    @Column(name = "PRODUCT_NAME")
    protected String productName;

    @ManyToOne
    @JoinColumn(name = "CONTACT_ID", nullable = false)
    protected Contact contact;

}
