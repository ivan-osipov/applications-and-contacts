package applications.api;

import applications.controllers.dto.ApplicationOutputDto;
import applications.model.Application;
import applications.model.Contact;
import applications.repositories.ApplicationsRepository;
import applications.repositories.ContactRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.val;
import lombok.var;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactsApiTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ApplicationsRepository applicationsRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ObjectMapper mapper;

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void shouldFindLastApplicationByContact() throws Exception {
        val contact = contactRepository.save(new Contact());
        var application = new Application();
        application.setContact(contact);
        application.setCreatedAt(LocalDateTime.of(2018, 11, 1, 11, 0));
        application.setProductName("TProduct");
        applicationsRepository.save(application);

        application = new Application();
        application.setContact(contact);
        application.setCreatedAt(LocalDateTime.of(2018, 11, 1, 12, 0));
        application.setProductName("TProduct2");
        application = applicationsRepository.save(application);

        val expectingJson = mapper.writeValueAsString(ApplicationOutputDto.create(application));

        val result = mockMvc.perform(get("/api/v1/contact/{contact_id}/applications/last", contact.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectingJson));

        result.andDo(document("{ClassName}/{methodName}", responseFields(
                fieldWithPath("CONTACT_ID").description("Requested contact unique identifier"),
                fieldWithPath("APPLICATION_ID").description("Found application unique identifier"),
                fieldWithPath("DT_CREATED").description("Creation date of a found application"),
                fieldWithPath("PRODUCT_NAME").description("Name of an application product")
        ), pathParameters(parameterWithName("contact_id").description("Contact id for application search"))));
    }

    @Test
    public void shouldFindNothingByCorrectContact() throws Exception {
        val contact = contactRepository.save(new Contact());

        val result = mockMvc.perform(get("/api/v1/contact/{contact_id}/applications/last", contact.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        result.andDo(document("{ClassName}/{methodName}", pathParameters(parameterWithName("contact_id").description("Contact id for application search"))));
    }

    @Test
    public void shouldReturnErrorByIncorrectContact() throws Exception {

        val result = mockMvc.perform(get("/api/v1/contact/{contact_id}/applications/last", UUID.randomUUID())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        result.andDo(document("{ClassName}/{methodName}", pathParameters(parameterWithName("contact_id").description("Contact id for application search"))));
    }

    @After
    public void tearDown() {
        applicationsRepository.deleteAll();
        contactRepository.deleteAll();
    }
}
